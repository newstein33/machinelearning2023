Betimsel İstatistik;
kovaryans: iki değişken arasındaki ilişkinin değişkenlik ölçüsüdür.

varyans: ortalama etrafındaki dağılımın ölçüsüdür, standart sapma karesi alındığında varyans elde edilir.

güven aralığı: anakütle parametresinin tahmini değerini kapsayabilecek iki sayıdan oluşan bir aralık bulunmasıdır
import statsmodels.stats.api as ssa
ssa.DecrStatsW(değişken).tconfint_mean() ile bulunur.

Thu Sep 30 2021 09:43:16 GMT+0300 (GMT+03:00)
https://www.udemy.com/course/python-egitimi/learn/lecture/17585038#overview