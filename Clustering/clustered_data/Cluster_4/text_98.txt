Lojistik Regresyon bir sınıflandırma problemi çözmek için kullanılan bir yöntemdir Bu yöntem veri setindeki noktaların hangi sınıfa ait olduğunu belirlemek için bir doğrusal fonksiyon kullanır Lojistik regresyon modelinin parametreleri
w_ w_  w_n  ağırlıklar veri noktasının özelliklerine göre modelin çıktısını etkileyebilir
b  bias modelin çıktısını etkileyebilir
Bu parametreler modelin eğitim sırasında optimize edilir ve veri setindeki noktaları en iyi şekilde sınıflandıran değerlere getirilir Lojistik Regresyon modeli için kullanılan optimizasyon fonksiyonları arasında en yaygın olarak kullanılanı crossentropy loss çaprazentropi kaybı fonksiyonudur Bu fonksiyon veri setindeki noktaların gerçek sınıfı ile tahmin edilen sınıf arasındaki farkı ölçer ve bu farkı minimuma indirmeye çalışır Ayrıca gradient descent optimizasyon algoritması kullanılır ve modelin parametreleri optimize edilir
Fri Jan    GMT GMT
httpschatopenaicomchat__cf_chl_tkhbwnVxBiEPIauCsZclxAaEZoMe_tIbEhYogaNycGzNEE