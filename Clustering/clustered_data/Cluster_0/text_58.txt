dönüşümler
 StandartScaler Standardizasyon işleminde her bir gözlem birimi ortalamadan çıkarılır ve standart sapmaya bölünür böylelikle değişkenler ortalaması  varyansı  olan normal dağılım elde edilir ancak bu yöntem verinin normal dağıldığı varsayımına dayanır işleme geçmeden önce verinin dağılımını kontrol etmelisiniz
bu dönüşümden sonra veriler  ve  arası değer alırlar
 normalizasyon normalizasyon işleminde verinin varyansı ortalamasını ve dağılımını değiştirmez bu işlemden sonra veriler  ve  arası değer alırlar
from sklearn import preprocessing
preprocessingnormalizesütunsütun
 Min Max Scaler veri setinde minimum ve maksimum değerleri bulur daha sonra her bir gözlem birimini minimum değerden çıkarır ve maksimum minimum değerine böler burada diğer yöntemlerin aksine feature_rangeminmax parametresini kullanarak değişkenlerin ölçekleneceği aralığı kendimiz belirleyebilmemiz bu parametreyi boş bırakırsak default olarak değişkenleri  ve  aralığında ölçeklendirir
 MaxAbs Scaler maxabs ölçekleme yönteminde öncelikle sütundaki her bir değişkenin mutlak değeri bulunur daha sonra mutlak değerce maksimum değer tespit edilir ve tüm gözlem birimleri tespit edilen mutlak maksimum değere bölünür veriler  ve  aralığında ölçeklenir
 Robust Scaler diğer yöntemler min ve maks değerleri kullandığından aykırı değerlere duyarlıydı ancak Robust scaler ölçeklendirmede IQR çeyrekler açıklığı kullandığı için aykırı değerlere dayanıklıdır
 Quantile Transformer Scaler quantile transformer scaler önce değişkeni normal dağılıma dönüştürür daha sonra ölçeklendirir bu yöntemde dikkat edilmesi gereken notka dağılımın değiştiğidir
NOT quantile transformer scalerda çıktı dağılımını normal veya uniform olarak belirleyebiliriz varsayılan değer uniformdur
 Power Transformer Scaler normalliğin istendiği durumlarda kullanılır
değişken varyans durumunda varyansı stabilize etmek ve verideki çarpıklığı azaltmak için maksimum olabilirlik yöntemi kullanılır
BOXCOX ve YEOJOHNSON dönüşümünü destekler
Kategorik dönüşümler

One Hot Encoder
Ordinal Encoderkategoriler arasında sıralama varsa
Binarize Dönüşüm önce labelencoder ile sayısallaştırır sonra binarize dönüşüm yaparız
pip install categoryencoders
from category_encoders import BinaryEncoder
bin  BinaryEncodercolsengTypedrivebodyreturn_dfTrue

Sat Sep    GMT GMT