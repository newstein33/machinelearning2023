aykırı değer işleme
aykırı gözlem türleri
 nokta aykırı değer bir gözlemin dikkat çekecek şekilde diğer durumlardan ayrıldığı değer durumu
 bağlamsal aykırı değer  öznitelik bakımından incelendiğinde bir sorun olmayıp başka durumlarla birlikte aykırılaşan değer durumu
 toplu aykırı değer Veri setinde belirli bir grup içinde normal davranış gösteren ancak tüm veri seti incelendiğinde aykırı olarak belirlenen değişkenler Kapanan tüm hastanelerin Çankaya ilçesinde yer alması
Nasıl tespit edilir

grafikler ile birlikte
histogram

boxplot


istatistiksel

çeyrekler açıklığı
z skoruZ Skoru yöntemini kullanmak için değişkenlerin normal dağılımdan geldiğinden emin olmamız gerekir Bu yöntemde değişkenin ölçeği değiştirilerek ortalamanın  stdsapma dışında kalan gözlemler belirlenerek aykırı gözlem olarak kabul edilir Z  x  μ  σ
standart sapma Gözlemlerin ortalamanın kaç standart sapma uzağında olduğu kontrol edilir  standart sapma uzakta kalan alt ve üst gözlemler aykırı değer olarak tanımlanır
ortanca mutlak sapmaBu yöntem standart sapma yöntemine benzer bir yöntemdir Tek fark burada değişkenlerin ortalamadan uzaklığına değil medyandan uzaklığına bakılmasıdır

Çok değişkenli aykırı değer tespiti
   grafikselscatterplot
   DBSCAN KMeanse alternatif bir kümeleme tekniğidir ve öncesinde küme sayısı belirtilmesine ihtiyaç yoktur Bu yöntemde öznitelikler bakımından benzer olan gözlemler bir araya getirilerek kümeleme yöntemiyle analiz edilir Kümeleme sonucunda  ve  değerleri döner  değeri kümelenemeyen gözlemleri temsil ederken  değeri ise kümelenmiş değişkenleri ifade eder
   LOFT LOFT yönteminde gözlemler bulundukları konumda yoğunluk tabanlı skorlanır ve aykırı olabilecek gözlemler tanımlanır Buradaki noktaların local yoğunluğu komşularıyla karşılaştırılır Eğer gözlem birimi komşularından anlamlı düzeyde farklılaşmışsa aykırı gözlem olarak belirlenir
   İzolasyon ormanı Bu yöntemde amaç karar ağaçlarında olduğu gibi değişkenin köküne inerek aykırı gözlemleri kaçırmadan tespit edebilmektir İzolasyon ormanı ile aykırı değer tespitinde belirli kriterlere dayanarak gözlemler için bir anormallik puanı hesaplanır Puan  ve  değerlerini alır  değerini alan gözlemler aykırı gözlem olarak belirlenir
Makine Öğrenmesine dayalı aykırı değer tespiti
   Elliptic Envelope gözlemler etrafında eliptik bir çizgi oluşturularak iç gözlemler ve uç gözlemler birbirinden ayrılır Çizgi içinde kalan gözlemler iç gözlem dışarıda kalanlar ise aykırı gözlemlerdir Burada contamination parametresine dikkat etmek gerekir Bu parametre veri setindeki aykırı gözlem oranını temsil eder Eğer herhangi bir değer girilmezse analizde  değeri kullanılır Normalde  arasında değer alabilir eğer aykırı gözlem oranınız fazlaysa contamination değerini yüksek almalısınız not Veri setindeki aykırı gözlem oranını bilmiyorsanız bu yöntemin kullanılması pek önerilmez
Aykırı gözlemlere ne yapılabilir
 silinebilir
 doldurulabilir
 baskılanabilir
 göz ardı edilebilir
aykırı değerlere dayanıklı algoritmalar
 Karar Ağaçları
 Random Forest
 XGBoost
 Naive Bayes
Kaynakça
Sun Sep    GMT GMT